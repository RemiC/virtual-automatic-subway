package org.duckdns.reman.vas.model

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.duckdns.reman.vas.exception.IllegalDoorStatusException
import org.duckdns.reman.vas.exception.UndefinedMotionDirectionException
import org.junit.Test
import kotlin.test.assertEquals

val legalDoorNumber : IntArray = intArrayOf(2,4,6,8,10)
val randomDoorNumberIndex: Int = (0..4).random()
val doorsPerSideNumber: Int = legalDoorNumber[randomDoorNumberIndex]
val carsNumber: Int = legalDoorNumber[(0..1).random()]

class TrainTest {

    private var train: Train = Train(carsNumber, doorsPerSideNumber)

    @Test
    fun openRightDoorsTest() = runBlocking()
    {
        assertEquals(false, train.isOneRightDoorOpen())
        train.forwardMotion = true
        train.openDoors(true)
        delay(6*1000)
        assertEquals(true, train.isOneRightDoorOpen())
        assertEquals(false, train.isOneLeftDoorOpen())
    }

    @Test
    fun openLeftDoorsTest() = runBlocking()
    {
        assertEquals(false, train.isOneRightDoorOpen())
        train.forwardMotion = false
        train.openDoors(true)
        delay(6*1000)
        assertEquals(true, train.isOneLeftDoorOpen())
        assertEquals(false, train.isOneRightDoorOpen())
    }

    @Test
    fun closeRightDoorsTest() = runBlocking()
    {
        assertEquals(false, train.isOneRightDoorOpen())
        train.forwardMotion = true
        train.openDoors(true)
        delay(6*1000)
        assertEquals(true, train.isOneRightDoorOpen())
        assertEquals(false, train.isOneLeftDoorOpen())
        train.closeDoors(true)
        delay(6*1000)
        assertEquals(false, train.isOneRightDoorOpen())
        assertEquals(false, train.isOneLeftDoorOpen())
    }

    @Test
    fun closeLeftDoorsTest() = runBlocking()
    {
        assertEquals(false, train.isOneLeftDoorOpen())
        train.forwardMotion = true
        train.openDoors(false)
        delay(6*1000)
        assertEquals(false, train.isOneRightDoorOpen())
        assertEquals(true, train.isOneLeftDoorOpen())
        train.closeDoors(false)
        delay(6*1000)
        assertEquals(false, train.isOneRightDoorOpen())
        assertEquals(false, train.isOneLeftDoorOpen())
    }

    @Test(expected = IllegalArgumentException::class)
    fun indivisibleDoorNumberThrowException(){
        Train(3, 4)
    }

    @Test(expected = UndefinedMotionDirectionException::class)
    fun openDoorsWhileMotionDirectionIsUndefinedThrowException(){
        train.openDoors(true)
    }

    @Test(expected = UndefinedMotionDirectionException::class)
    fun closeDoorsWhileMotionDirectionIsUndefinedThrowException(){
        train.closeDoors(true)
    }

    @Test(expected = IllegalDoorStatusException::class)
    fun openRightDoorsWhileLeftOpenThrowException() = runBlocking()
    {
        assertEquals(false, train.isOneRightDoorOpen())
        train.forwardMotion = true
        train.openDoors(true)
        delay(6*1000)
        train.forwardMotion = false
        train.openDoors(true)
        delay(6*1000)
    }

    @Test(expected = IllegalDoorStatusException::class)
    fun openLeftDoorsWhileRightOpenThrowException() = runBlocking()
    {
        assertEquals(false, train.isOneRightDoorOpen())
        train.forwardMotion = false
        train.openDoors(true)
        delay(6*1000)
        train.forwardMotion = true
        train.openDoors(true)
        delay(6*1000)
    }
}