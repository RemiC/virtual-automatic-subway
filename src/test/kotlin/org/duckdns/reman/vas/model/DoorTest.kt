package org.duckdns.reman.vas.model

import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertTrue

const val closingTime: Long = 4*1000


class DoorTest {
    private val door: Door = Door()

    @Test
    fun openDoorTest() = runBlocking{
        assertTrue {
            !door.open
        }
        door.openDoor()
        assertTrue {
            door.open
        }
    }

    @Test
    fun closeDoorTest() = runBlocking{
        door.closeDoor()
        assertTrue {
            !door.open
        }
    }
}