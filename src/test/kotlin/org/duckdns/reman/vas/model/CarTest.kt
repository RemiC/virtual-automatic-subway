package org.duckdns.reman.vas.model

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Test

import kotlin.test.assertEquals

val doorsNumber = (1..10).random()

class CarTest {

    private val car: Car = Car(doorsNumber)

    @Test
    fun openRightDoorsTest() = runBlocking {
        assertEquals(false, car.isOneDoorOpen())
        car.openRightDoors()
        delay(closingTime)
        assertEquals(false, car.isOneRightDoorClosed())
        assertEquals(false, car.isOneLeftDoorOpen())
    }

    @Test
    fun openLeftDoorsTest() = runBlocking {
        assertEquals(false, car.isOneDoorOpen())
        car.openLeftDoors()
        delay(closingTime)
        assertEquals(false, car.isOneLeftDoorClosed())
        assertEquals(false, car.isOneRightDoorOpen())
    }

    @Test
    fun closeRightDoorsTest() = runBlocking {
        car.openRightDoors()
        car.openLeftDoors()
        delay(closingTime)
        assertEquals(false, car.isOneDoorClosed())
        car.closeRightDoors()
        delay(closingTime)
        assertEquals(false, car.isOneRightDoorOpen())
        assertEquals(false, car.isOneLeftDoorClosed())
    }

    @Test
    fun closeLeftDoorsTest() = runBlocking {
        car.openRightDoors()
        car.openLeftDoors()
        delay(closingTime)
        assertEquals(false, car.isOneDoorClosed())
        car.closeLeftDoors()
        delay(closingTime)
        assertEquals(false, car.isOneLeftDoorOpen())
        assertEquals(false, car.isOneRightDoorClosed())
    }

    @Test
    fun isOneRightDoorOpenTest() = runBlocking{
        assertEquals(false, car.isOneRightDoorOpen())
        car.openRightDoors()
        delay(closingTime)
        assertEquals(true, car.isOneRightDoorOpen())
    }

    @Test
    fun isOneLeftDoorOpenTest() = runBlocking {
        assertEquals(false, car.isOneLeftDoorOpen())
        car.openLeftDoors()
        delay(closingTime)
        assertEquals(true, car.isOneLeftDoorOpen())
    }

    @Test
    fun isOneRightDoorClosed() = runBlocking{
        assertEquals(true, car.isOneRightDoorClosed())
        car.openRightDoors()
        delay(closingTime)
        assertEquals(false, car.isOneRightDoorClosed())
    }

    @Test
    fun isOneLeftDoorClosed() = runBlocking{
        assertEquals(true, car.isOneLeftDoorClosed())
        car.openLeftDoors()
        delay(closingTime)
        assertEquals(false, car.isOneLeftDoorClosed())
    }
}