package org.duckdns.reman.vas

import org.duckdns.reman.vas.exception.IllegalDoorStatusException
import org.duckdns.reman.vas.model.Train

fun main(){
    println("Hello World")

    val train = Train(2, 6)
    train.forwardMotion = true

    try {
        train.openDoors(false)
    } catch (e: IllegalDoorStatusException){
        e.printStackTrace()
    }


    Thread.sleep(7*1000)

    train.forwardMotion = false
    try {
        train.openDoors(false)
    } catch (e: IllegalDoorStatusException){
        e.printStackTrace()
    }

    Thread.sleep(7*1000)
}