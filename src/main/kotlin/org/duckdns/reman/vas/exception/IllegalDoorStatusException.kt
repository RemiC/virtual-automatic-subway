package org.duckdns.reman.vas.exception

class IllegalDoorStatusException(message: String): Exception(message) {
}