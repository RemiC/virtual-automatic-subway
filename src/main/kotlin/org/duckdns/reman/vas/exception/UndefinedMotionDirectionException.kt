package org.duckdns.reman.vas.exception

class UndefinedMotionDirectionException(message: String): Exception(message)