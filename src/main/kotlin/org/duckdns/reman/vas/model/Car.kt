package org.duckdns.reman.vas.model

import kotlinx.coroutines.*

class Car(doorNumberPerSide: Int){

    private var leftDoors: ArrayList<Door> = ArrayList()
    private var rightDoors: ArrayList<Door> = ArrayList()

    init {

        for(i in 0 until doorNumberPerSide){
            addDoorsPair()
        }

        println("door number left size" + leftDoors.size)
        println("door number right size" + rightDoors.size)
    }

    private fun addDoorsPair(){
            /*launch {
                leftDoors.add(Door())
            }
            launch {
                rightDoors.add(Door())
            }*/
        leftDoors.add(Door())
        rightDoors.add(Door())
        /*leftDoors.add(async {
            Door()
        })

        rightDoors.add(async {
            Door()
        })*/

    }

    fun openRightDoors(){

        for (door in rightDoors){
            GlobalScope.launch {
                door.openDoor()
            }
        }
    }

    fun openLeftDoors(){
        for (door in leftDoors){
            GlobalScope.launch {
                door.openDoor()
            }
        }
    }

    fun closeRightDoors(){
        for (door in rightDoors){
            GlobalScope.launch {
                door.closeDoor()
            }
        }
    }
    fun closeLeftDoors(){
        for (door in leftDoors){
            GlobalScope.launch {
                door.closeDoor()
            }
        }
    }

     fun isOneRightDoorOpen(): Boolean {
         var check = false
         for(door in rightDoors){
             check = check || door.open
         }
        return check
    }

    fun isOneLeftDoorOpen(): Boolean{
        var check = false
        for(door in leftDoors){
            check = check || door.open
        }
        return check
    }

    fun isOneRightDoorClosed(): Boolean {
        var check = false
        for(door in rightDoors){
            check = check || !door.open
        }
        return check
    }

    fun isOneLeftDoorClosed(): Boolean{
        var check = false
        for(door in leftDoors){
            check = check || !door.open
        }
        return check
    }

    fun isOneDoorOpen(): Boolean
    {
        return isOneLeftDoorOpen() || isOneRightDoorOpen()
    }
    fun isOneDoorClosed(): Boolean
    {
        return isOneLeftDoorClosed() || isOneRightDoorClosed()
    }

}