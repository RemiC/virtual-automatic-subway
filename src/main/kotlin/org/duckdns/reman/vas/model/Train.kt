package org.duckdns.reman.vas.model

import org.duckdns.reman.vas.exception.IllegalDoorStatusException
import org.duckdns.reman.vas.exception.UndefinedMotionDirectionException

class Train(numberOfCar: Int, numberOfDoorsPerSide: Int) {

    var cars: ArrayList<Car> = ArrayList()
    private set

    var forwardMotion: Boolean? = null

    init {
        if(numberOfDoorsPerSide % numberOfCar == 0) {
            val numberOfDoorsPerCarSide = numberOfDoorsPerSide / numberOfCar
            for(i in 0 until numberOfCar){
                cars.add(Car(numberOfDoorsPerCarSide))
            }
        } else {
            throw IllegalArgumentException("numberOfDoorsPerSide must be divisible by the number of car")
        }
    }

      fun openDoors(centralPlatform: Boolean){
        if(forwardMotion != null){
            if(centralPlatform){
                if(forwardMotion == true){
                    openRightDoors()
                } else {
                    openLeftDoors()
                }
            } else {
                if(forwardMotion == true){
                    openLeftDoors()
                } else {
                    openRightDoors()
                }
            }
        } else {
            throw UndefinedMotionDirectionException("set motion direction before trying to close the doors")
        }
    }

    fun closeDoors(centralPlatform: Boolean){
        if(forwardMotion != null){
            if(centralPlatform){
                if(forwardMotion == true){
                    closeRightDoors()
                } else {
                    closeLeftDoors()
                }
            } else {
                if(forwardMotion == true){
                    closeLeftDoors()
                } else {
                    closeRightDoors()
                }
            }
        } else {
            throw UndefinedMotionDirectionException("set motion direction before trying to close the doors")
        }
    }

    private fun openRightDoors(){
        if(!isOneLeftDoorOpen()) {
            for (car in cars) {
                car.openRightDoors()
            }
        } else
        {
            throw IllegalDoorStatusException("One left door is already open. Close it before opening the right side")
        }
    }

    private fun openLeftDoors(){
        if(!isOneRightDoorOpen()) {
            for (car in cars) {
                car.openLeftDoors()
            }
        } else {
            throw IllegalDoorStatusException("One right door is already open. Close it before opening the left side")
        }
    }

    private fun closeRightDoors(){
        for(car in cars){
            car.closeRightDoors()
        }
    }

    private fun closeLeftDoors(){
        for(car in cars){
            car.closeLeftDoors()
        }
    }

     fun isOneRightDoorOpen(): Boolean{
        var check = false
         var boolean: Boolean
        for(car in cars){
            boolean = car.isOneRightDoorOpen()
            check = check || boolean
        }
        return check
    }

    fun isOneLeftDoorOpen(): Boolean{
        var check = false
        var boolean: Boolean
        for(car in cars){
            boolean = car.isOneLeftDoorOpen()
            check = check || boolean
        }
        return check
    }
}