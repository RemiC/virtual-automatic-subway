package org.duckdns.reman.vas.model

import kotlinx.coroutines.delay

private const val closingTime: Long = 3

class Door {

    var open: Boolean = false
    private set

    var error: Boolean =  false
    private set

    var locked: Boolean = true
    private set

    private fun unlockDoor(): Boolean{
        locked = false
        return !locked
    }

    private fun lockDoor(): Boolean{
        locked = true
        return locked
    }

    suspend fun openDoor(){
        if (!open){
            while(!unlockDoor()){
                println("unlocking door")
            }
            println("Opening door")
            delay(closingTime*1000)
            open = true
            println("Door is open")
        }
    }

    suspend fun closeDoor(){
        if (open){
            println("closing door")
            delay(closingTime*1000)
            open = false
            while(!lockDoor()){
                println("locking door")
            }
            println("Door is closed")
        }
    }
}